package com.opnd.dl.mStuff;

import java.sql.Date;
import java.text.DateFormat;
import java.time.Instant;

public class LogUtil {

    public static void log(String activity) {
        String logString = DateFormat.getDateInstance().format(Date.from(Instant.now())) +
                ", " +
                DateFormat.getTimeInstance().format(java.util.Date.from(Instant.now())) +
                "\t:\t" + activity;
        System.out.println(logString);
    }
}
