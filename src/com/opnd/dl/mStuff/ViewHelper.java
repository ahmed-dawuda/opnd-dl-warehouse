package com.opnd.dl.mStuff;

import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;

public class ViewHelper {
    private double xOffset = 0;
    private double yOffset = 0;
    private Alert alert = new Alert(Alert.AlertType.NONE);

    public Alert msgBox(Alert.AlertType type, String title, String header, String content) {
        alert.setAlertType(type);
        alert.setTitle(title);
        alert.setHeaderText(header);
        alert.setContentText(content);
        return alert;
    }

    public void switchScene(String fxml, boolean resizeable, Node pivot) {
        fxml = "/com/opnd/dl/view/responsive/" + fxml + ".fxml";
        Stage primaryStage = (Stage) pivot.getScene().getWindow();
        primaryStage.hide();

        try {
            Parent root = FXMLLoader.load(getClass().getResource(fxml));
            root.setOnMousePressed(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    xOffset = event.getSceneX();
                    yOffset = event.getSceneY();
                }
            });
            root.setOnMouseDragged(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    primaryStage.setX(event.getScreenX() - xOffset);
                    primaryStage.setY(event.getScreenY() - yOffset);
                }
            });
            primaryStage.setScene(new Scene(root));
            primaryStage.setResizable(resizeable);
            primaryStage.sizeToScene();
            primaryStage.show();
            primaryStage.setMinHeight(primaryStage.getHeight());
            primaryStage.setMinWidth(primaryStage.getWidth());
            primaryStage.setMaximized(resizeable);

        } catch (IOException e) {
            e.printStackTrace();
            msgBox(Alert.AlertType.ERROR,
                    "Error",
                    "FXML Loading Error",
                    "An error occurred:\n" + e.getMessage())
                    .show();
        }
    }

    public void switchRoot(String fxml, Node node){
        try {
            fxml = "/com/opnd/dl/view/responsive/" + fxml + ".fxml";
            Parent newRoot = null;
            newRoot = FXMLLoader.load(getClass().getResource(fxml));
            node.getScene().setRoot(newRoot);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void makeModal(String fxml) {
        fxml = "/com/opnd/dl/view/responsive/" + fxml + ".fxml";
        Stage modalStage = new Stage();
        modalStage.initModality(Modality.APPLICATION_MODAL);

        try {
            Parent root = FXMLLoader.load(getClass().getResource(fxml));
            modalStage.setScene(new Scene(root));
            modalStage.sizeToScene();
            modalStage.showAndWait();
        } catch (IOException e) {
            e.printStackTrace();
            msgBox(Alert.AlertType.ERROR,
                    "Error",
                    "FXML Loading Error",
                    "An error occurred:\n" + e.getMessage())
                    .show();
        }
    }
}
