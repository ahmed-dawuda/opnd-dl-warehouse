package com.opnd.dl.model;

import com.google.gson.Gson;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.opnd.dl.domain.Category;
import com.opnd.dl.domain.Product;
import com.opnd.dl.mStuff.GlobalValues;
import com.opnd.dl.mStuff.Persistor;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class CategoryUtil {

//    private String holderId = GlobalValues.getCurrentSubject().getEmail();
//    private String key = GlobalValues.getCurrentSubject().getKey();
    private final String BASE_URL = GlobalValues.getBaseUrl();

    private List<String> localFetchIds() {
        List<String> ids = new ArrayList<>();
        List l = Persistor.fetch("select c.id from Category c");

        for (Object o : l) {
            ids.add((String) o);
        }

        return ids;
    }

    private Category localFetchCategory(String id) {
        return (Category) Persistor.find(Category.class, id);
    }

    private List<String> remoteFetchIds() throws UnirestException {
        String url = BASE_URL + "categories/idsOnly";
        List<String> ids = new ArrayList<>();

        final HttpResponse<JsonNode> response = Unirest.get(url)
                .header("Authorization", GlobalValues.getCurrentSubject().getKey())
                .header("Accept", "application/json")
                .asJson();

        JSONObject object = response.getBody().getObject();
        if (object.has("status") && object.getBoolean("status")) {
            JSONArray idListJson = object.getJSONArray("message");
            for (int i = 0; i < idListJson.length(); i++) {
                ids.add(idListJson.getString(i));
            }
        }

        return ids;
    }

    private Category remoteFetchCategory(String id) throws UnirestException {
        String url = BASE_URL + "categories/" + id;

        final HttpResponse<JsonNode> response = Unirest.get(url)
                .header("Authorization", GlobalValues.getCurrentSubject().getKey())
                .header("Accept", "application/json")
                .asJson();

        JSONObject object = response.getBody().getObject();
        if (object.has("status") && object.getBoolean("status")) {
            JSONObject itemJson = object.getJSONObject("message");
            Gson gson = new Gson();
            return gson.fromJson(itemJson.toString(), Category.class);
        }

        return null;
    }



    private boolean downloadCategory(String id) throws UnirestException {
        Category item = remoteFetchCategory(id);

       if(item != null){
           try {
               item.setSynced(true);
               Persistor.insert(item);
               return true;
           } catch (Exception e) {
               e.printStackTrace();
           }
       }

        return false;
    }

    private boolean postCategory(String id) throws UnirestException {
        String url = BASE_URL + "categories";
        Category item = localFetchCategory(id);
        Map params = new ObjectMapper().convertValue(item, Map.class);

        final HttpResponse<JsonNode> response = Unirest.post(url)
                .header("Authorization", GlobalValues.getCurrentSubject().getKey())
                .header("Accept", "application/json")
                .fields(params)
                .asJson();

        JSONObject object = response.getBody().getObject();
        return object.has("status") && object.getBoolean("status");
    }

    public void sync() throws UnirestException {
        List<String> localList = localFetchIds();
        List<String> remoteList = remoteFetchIds();

        List<String> localOnly = new ArrayList<>();
        List<String> remoteOnly = new ArrayList<>();

//        for (Number number : localList) {
//            if (!remoteList.contains(number))
//                localOnly.add(number);
//        }

        for (String number : remoteList) {
            if (!localList.contains(number))
                remoteOnly.add(number);
        }

//        for (Number number : localOnly) {
//            postCategory(number);
//        }

        for (String number : remoteOnly) {
            downloadCategory(number);
        }
    }

    @SuppressWarnings("unchecked")
    private void postUnsynched() throws UnirestException{
        List<String> l = Persistor.fetch("select c.id from Category c where c.synced = ?1", false);
        for (String id : l) {
            if(postCategory(id)){
                Category c = localFetchCategory(id);
                Persistor.begin();
                c.setSynced(true);
                Persistor.commit();
            }
        }
    }


    public List<Category> allCategories(){
        return Persistor.fetchAll("Category");
    }

    public List<Product> getProducts(long id){
        return Persistor.fetch("select count(p) from Product p where p.categoryId = ?1", id);
    }
}
