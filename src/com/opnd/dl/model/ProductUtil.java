package com.opnd.dl.model;

import com.google.gson.Gson;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.opnd.dl.domain.Product;
import com.opnd.dl.domain.wrapper.ProductManagementWrapper;
import com.opnd.dl.mStuff.GlobalValues;
import com.opnd.dl.mStuff.LogUtil;
import com.opnd.dl.mStuff.Persistor;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ProductUtil {

    private String holderId = GlobalValues.getCurrentSubject().getId();
    private String key = GlobalValues.getCurrentSubject().getKey();
    private final String BASE_URL = GlobalValues.getBaseUrl();

    private List<String> localFetchIds() {
        List<String> ids = new ArrayList<>();
        List l = Persistor.fetch("select p.id from Product p");

        for (Object o : l) {
            ids.add((String) o);
        }

        return ids;
    }

    private Product localFetchProduct(String id) {
        return (Product) Persistor.find(Product.class, id);
    }

    private List<String> remoteFetchIds() throws UnirestException {
        String url = BASE_URL + "products/idsOnly";
        List<String> ids = new ArrayList<>();

        final HttpResponse<JsonNode> response = Unirest.get(url)
                .header("Authorization", key)
                .header("Accept", "application/json")
                .asJson();

        JSONObject object = response.getBody().getObject();
        if (object.has("status") && object.getBoolean("status")) {
            JSONArray idListJson = object.getJSONArray("message");
            for (int i = 0; i < idListJson.length(); i++) {
                ids.add(idListJson.getString(i));
            }
        }

        return ids;
    }

    private Product remoteFetchProduct(String id) throws UnirestException {
        String url = BASE_URL + "products/" + id;

        final HttpResponse<JsonNode> response = Unirest.get(url)
                .header("Authorization", key)
                .header("Accept", "application/json")
                .asJson();

        JSONObject object = response.getBody().getObject();
        if (object.has("status") && object.getBoolean("status")) {
            JSONObject itemJson = object.getJSONObject("message");
            Gson gson = new Gson();
            return gson.fromJson(itemJson.toString(), Product.class);
        }

        return null;
    }

    private boolean downloadProduct(String id) throws UnirestException {
        Product item = remoteFetchProduct(id);
        if (item != null) {
//            LogUtil.log(item.toString(2));
            try {
                Persistor.insert(item);
                Persistor.begin();
                item.setSynced(true);
                Persistor.commit();
                return true;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return false;
    }

    private boolean postProduct(String id) throws UnirestException {
        String url = BASE_URL + "products";
        Product item = localFetchProduct(id);
        Map params = new ObjectMapper().convertValue(item, Map.class);

        final HttpResponse<JsonNode> response = Unirest.post(url)
                .header("Authorization", key)
                .header("Accept", "application/json")
                .fields(params)
                .asJson();

        JSONObject object = response.getBody().getObject();
        return object.has("status") && object.getBoolean("status");
    }

    public void sync() throws UnirestException {
        List<String> localList = localFetchIds();
//        LogUtil.log(localList.toString());
        List<String> remoteList = remoteFetchIds();
//        LogUtil.log(remoteList.toString());

        List<String> localOnly = new ArrayList<>();
        List<String> remoteOnly = new ArrayList<>();

//        for (Number number : localList) {
//            if (!remoteList.contains(number))
//                localOnly.add(number);
//        }

        for (String number : remoteList) {
            if (!localList.contains(number))
                remoteOnly.add(number);
        }
//        LogUtil.log(remoteOnly.toString());

//        for (Number number : localOnly) {
//            postProduct(number);
//        }

        for (String number : remoteOnly) {
            downloadProduct(number);
        }
    }

    @SuppressWarnings("unchecked")
    private void postUnsynced() throws UnirestException {
        List<String> l = Persistor.fetch("Select p.id from Product p where p.synced = ?1", false);
        for (String id : l) {
            if(postProduct(id)){
                Product p = localFetchProduct(id);
                Persistor.begin();
                p.setSynced(true);
                Persistor.commit();
            }
        }
    }


    public Product createProduct(String description, double price, String categoryId) {
        Product product = new Product();
        product.setDescription(description);
        product.setPrice(price);
        product.setCategoryId(categoryId);
        Persistor.insert(product);
        return product;
    }

    public Product modProduct(String id, String description, double price, String categoryId) {
        Persistor.begin();

        Product product = (Product) Persistor.find(Product.class, id);
        product.setDescription(description);
        product.setPrice(price);
        product.setCategoryId(categoryId);

        Persistor.commit();
        return product;
    }

    public boolean deleteProduct(Product product) {
        try {
            Persistor.begin();
            product.setDeleted(true);
            Persistor.commit();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    public List<Product> allProducts() {
        List<Product> products = Persistor.fetch("Select p from Product p where p.id in ( Select i.productId from InventoryItem i )");
        return products;
    }

    public List<Product> products(){
        return Persistor.fetchAll("Product");
    }

    public List<ProductManagementWrapper> allWrappedProducts(){
        List<Product> products = this.allProducts();
        List<ProductManagementWrapper> productManagementWrappers = new ArrayList<>();
        for(Product prod : products){
            productManagementWrappers.add(new ProductManagementWrapper(prod));
        }
        return productManagementWrappers;
    }



}
