package com.opnd.dl.model;

import com.google.gson.Gson;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.opnd.dl.domain.Role;
import com.opnd.dl.domain.Subject;
import com.opnd.dl.mStuff.GlobalValues;
import com.opnd.dl.mStuff.LogUtil;
import com.opnd.dl.mStuff.Persistor;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class UserUtil {

    private final String BASE_URL = GlobalValues.getBaseUrl();


    private List<String> localFetchIds() {
        List<String> ids = new ArrayList<>();
        List l = Persistor.fetch("select s.id from Subject s");

        for (Object o : l) {
            ids.add((String) o);
        }

        return ids;
    }

    private Subject localFetchSubject(String id) {
        return (Subject) Persistor.find(Subject.class, id);
    }

    private List<String> remoteFetchIds() throws UnirestException {
        String url = BASE_URL + "users/idsOnly";
        List<String> ids = new ArrayList<>();

        final HttpResponse<JsonNode> response = Unirest.get(url)
                .header("Authorization", GlobalValues.getCurrentSubject().getKey())
                .header("Accept", "application/json")
                .asJson();

        JSONObject object = response.getBody().getObject();
        if (object.has("status") && object.getBoolean("status")) {
            JSONArray idListJson = object.getJSONArray("message");
            for (int i = 0; i < idListJson.length(); i++) {
                ids.add(idListJson.getString(i));
            }
        }

        return ids;
    }

    private Subject remoteFetchSubject(String id) throws UnirestException {
        String url = BASE_URL + "users/" + id;

        final HttpResponse<JsonNode> response = Unirest.get(url)
                .header("Authorization", GlobalValues.getCurrentSubject().getKey())
                .header("Accept", "application/json")
                .asJson();

        JSONObject object = response.getBody().getObject();
        LogUtil.log(object.toString(2));

        if (object.has("status") && object.getBoolean("status")) {
            JSONObject itemJson = object.getJSONObject("message");
            Gson gson = new Gson();
            LogUtil.log(gson.fromJson(itemJson.toString(), Subject.class).toString());
            return gson.fromJson(itemJson.toString(), Subject.class);
        }

        return null;
    }

    public Subject[] downloadSubjects() throws UnirestException {
        String url = BASE_URL + "users";
        LogUtil.log("downloadSubjects() is running...");

        final HttpResponse<JsonNode> response = Unirest.get(url)
                .header("Authorization", GlobalValues.getCurrentSubject().getKey())
                .header("Accept", "application/json")
                .asJson();

        JSONObject object = response.getBody().getObject();
        if (object.has("status") && object.getBoolean("status")) {
            JSONArray itemJson = object.getJSONArray("message");
            Gson gson = new Gson();
            return gson.fromJson(itemJson.toString(), Subject[].class);
        }
        return null;
    }

    private boolean downloadSubject(String id) throws UnirestException {
        Subject item = remoteFetchSubject(id);
//        LogUtil.log("downloaded user: "+item.toString());
        if(item != null){
            item.setSynced(true);
            try {
                Persistor.insert(item);
                return true;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return false;
    }

    private boolean postSubject(String id) throws UnirestException {
        String url = BASE_URL + "register";
        Subject item = localFetchSubject(id);
        Map params = new ObjectMapper().convertValue(item, Map.class);

        final HttpResponse<JsonNode> response = Unirest.post(url)
                .header("Authorization", GlobalValues.getCurrentSubject().getKey())
                .header("Accept", "application/json")
                .fields(params)
                .asJson();

        JSONObject object = response.getBody().getObject();
        LogUtil.log(object.toString(2));
        boolean posted = object.has("status") && object.getBoolean("status");
        if(posted){
            Persistor.begin();
            item.setSynced(true);
            Persistor.commit();
        }
        return posted;
    }

    public void sync() throws UnirestException {
        List<String> localList = localFetchIds();
        List<String> remoteList = remoteFetchIds();

        List<String> localOnly = new ArrayList<>();
        List<String> remoteOnly = new ArrayList<>();

//        for (Number number : localList) {
//            if (!remoteList.contains(number))
//                localOnly.add(number);
//        }

        for (String number : remoteList) {
            if (!localList.contains(number))
                remoteOnly.add(number);
        }

//        for (Number number : localOnly) {
//            postSubject(number);
//        }

        for (String number : remoteOnly) {
            downloadSubject(number);
        }
//        if(localList.size() <= 2){
//            List<Subject> subjects = Arrays.asList(downloadSubjects());
//            for (Subject subject : subjects) {
//                if(!localList.contains(subject.getId())){
//                    subject.setSynced(true);
//                    Persistor.insert(subject);
//                }
//            }
//        }
    }

    public Subject login(String username, String password) {
//        createAdmin();

        Subject current = localFetch(username);
        if (current != null)
            if (current.getPassword().equalsIgnoreCase(password))
                return current;

        try {
            current = remoteFetch(username, password);
        } catch (UnirestException e) {
            e.printStackTrace();
        }
        if (current != null) {
//            if (localFetch(current.getEmail()) == null) {
//                current.setId(0);
////                current.setEmail(username);
//                current.setPassword(password);
//                Role role = new RoleUtil().find(current.getRoleId());
//                if (role == null) {
//                    Role admin = (Role) Persistor.fetch("Select r from Role r where r.description = ?1","Admin").get(0);
//                    current.setRoleId(admin.getId());
//                }
//                Persistor.insert(current);
//            }
//            if (current.getPassword().equalsIgnoreCase(password))
            Persistor.insert(current);
            return current;
        }

        return null;
    }

    private Subject remoteFetch(String email, String password) throws UnirestException {
        final HttpResponse<JsonNode> response1 = Unirest.post(BASE_URL + "login")
                .header("Accept", "application/json")
                .field("email", email)
                .field("password", password)
//                .fields(m.convertValue(user, Map.class))
                .asJson();

        JSONObject object = response1.getBody().getObject();
        Subject subject = null;
        if (object.has("status") && object.getBoolean("status")) {
            JSONObject message = object.getJSONObject("message");
//            LogUtil.log(message.toString(2));
//            LogUtil.log(message.getInt("id")+"");

            Subject user = new Subject();
            user.setId(message.getString("id"));
            user.setSynced(true);
            user.setRoleId(message.getString("roleId"));
            user.setPassword(message.getString("raw_password"));
            user.setFirstname(message.getString("firstname"));
            user.setFullname(message.getString("fullname"));
            user.setEmail(message.getString("email"));
            user.setKey(message.getString("key"));
            user.setLocationId(message.getString("locationId"));
//            user = new Gson().fromJson(message.toString(), Subject.class);
            subject = user;
//            LogUtil.log(user.getId()+"");
        }

//        LogUtil.log(object.toString(2));
        return subject;
    }


    private Subject remoteFetch(String username) {


        return null;
    }

    private Subject localFetch(String email) {
        List l = Persistor.fetch("select s from Subject s where s.email = ?1", email);
        if (l.size() == 0) return null;
        else return (Subject) l.get(0);
    }

    public Subject createSubject(String firstname, String middlename, String lastname, String level, String password) {
        Subject subject = new Subject();
        subject.setFirstname(firstname);

        StringBuilder builder = new StringBuilder(lastname);
        if (!middlename.isEmpty()) builder.append(" ").append(middlename);
        builder.append(" ").append(firstname);
        subject.setFullname(builder.toString());
        subject.setEmail(Character.toLowerCase(firstname.charAt(0)) + lastname.toLowerCase());
        subject.setRoleId(level);
        subject.setPassword(password);

        Persistor.insert(subject);

        return subject;
    }

    private Subject modSubject(String username, String firstname, String middlename, String lastname, String level, String password) {
        Persistor.begin();

        Subject subject = (Subject) Persistor.find(Subject.class, username);
        subject.setFirstname(firstname);

        StringBuilder builder = new StringBuilder(lastname);
        if (!middlename.isEmpty()) builder.append(" ").append(middlename);
        builder.append(" ").append(firstname);
        subject.setFullname(builder.toString());
        subject.setRoleId(level);
        subject.setPassword(password);

        Persistor.commit();

        return subject;
    }

    private void createAdmin() {


        List list = Persistor.fetchAll("Subject");
        if (list.size() > 0) return;

        Role role = new Role();
        role.setDescription("Admin");
        Persistor.insert(role);

        Subject admin = createSubject("Admin", "", "Admin", role.getId(), "admin");

//        Persistor.insert(admin);
    }

    public Role getRole(Long roleId){
        return (Role) Persistor.find(Role.class, roleId);
    }

    public List<Subject> allSubject(){
        return Persistor.fetchAll("Subject");
    }
}
