package com.opnd.dl.model;

import com.google.gson.Gson;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.opnd.dl.domain.InventoryItem;
import com.opnd.dl.domain.Product;
import com.opnd.dl.domain.SaleContent;
import com.opnd.dl.domain.SaleItem;
import com.opnd.dl.domain.wrapper.SaleWrapper;
import com.opnd.dl.mStuff.GlobalValues;
import com.opnd.dl.mStuff.Persistor;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONObject;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class SaleUtil {

    private String holderId = GlobalValues.getCurrentSubject().getId();
    private String key = GlobalValues.getCurrentSubject().getKey();
    private final String BASE_URL = GlobalValues.getBaseUrl();

    private List<String> localFetchIds() {
        List<String> ids = new ArrayList<>();
        List l = Persistor.fetch("select s.id from SaleItem s");

        for (Object o : l) {
            ids.add((String) o);
        }

        return ids;
    }

    private SaleItem localFetchSaleItems(String id) {
        return (SaleItem) Persistor.find(SaleItem.class, id);
    }

    private List<String> remoteFetchIds() throws UnirestException {
        String url = BASE_URL + "saleItems/idsOnly";
        List<String> ids = new ArrayList<>();

        final HttpResponse<JsonNode> response = Unirest.get(url)
                .header("Authorization", key)
                .header("Accept", "application/json")
                .asJson();

        JSONObject object = response.getBody().getObject();
        if (object.has("status") && object.getBoolean("status")) {
            JSONArray idListJson = object.getJSONArray("message");
            for (int i = 0; i < idListJson.length(); i++) {
                ids.add(idListJson.getString(i));
            }
        }

        return ids;
    }

    private SaleItem remoteFetchSaleItem(String id) throws UnirestException {
        String url = BASE_URL + "saleItems/" + id + "/" + holderId;

        final HttpResponse<JsonNode> response = Unirest.get(url)
                .header("Authorization", key)
                .header("Accept", "application/json")
                .asJson();

        JSONObject object = response.getBody().getObject();
        if (object.has("status") && object.getBoolean("status")) {
            JSONObject itemJson = object.getJSONObject("message");
            Gson gson = new Gson();
            return gson.fromJson(itemJson.toString(), SaleItem.class);
        }

        return null;
    }

    private boolean downloadSaleItem(String id) throws UnirestException {
        SaleItem item = remoteFetchSaleItem(id);

        try {
            Persistor.insert(item);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    private boolean postSaleItem(String id) throws UnirestException {
        String url = BASE_URL + "saleItems";
        SaleItem item = localFetchSaleItems(id);
        Map params = new ObjectMapper().convertValue(item, Map.class);

        final HttpResponse<JsonNode> response = Unirest.post(url)
                .header("Authorization", key)
                .header("Accept", "application/json")
                .fields(params)
                .asJson();

        JSONObject object = response.getBody().getObject();
        return object.has("status") && object.getBoolean("status");
    }

    public void sync() throws UnirestException {
        List<String> localList = localFetchIds();
        List<String> remoteList = remoteFetchIds();

        List<String> localOnly = new ArrayList<>();
        List<String> remoteOnly = new ArrayList<>();

        for (String number : localList) {
            if (!remoteList.contains(number))
                localOnly.add(number);
        }

        for (String number : remoteList) {
            if (!localList.contains(number))
                remoteOnly.add(number);
        }

        for (String number : localOnly) {
            postSaleItem(number);
        }

        for (String number : remoteOnly) {
            downloadSaleItem(number);
        }
    }

    @SuppressWarnings("unchecked")
    public void postUnsynced() throws UnirestException{
        List<String> l = Persistor.fetch("Select s.id from SaleItem s where s.synced = ?1", false);
        for (String id : l) {
            if(postSaleItem(id)){
                SaleItem s = localFetchSaleItems(id);
                Persistor.begin();
                s.setSynced(true);
                Persistor.commit();
            }
        }
    }


    public List<SaleItem> listSaleItems(Date date) {
        List items = Persistor.fetch("select s from SaleItem s " +
                "where FUNC('DATE', s.date) = FUNC('DATE', ?1)", date);

        List<SaleItem> saleItems = new ArrayList<>();
        for (Object item : items) saleItems.add((SaleItem) item);

        return saleItems;
    }

    public List<SaleItem> listSaleItems(Date date, String holderId) {
        List items = Persistor.fetch("select s from SaleItem s " +
                "where FUNC('DATE', s.date) = FUNC('DATE', ?1) and s.holderId = ?2", date, holderId);

        List<SaleItem> saleItems = new ArrayList<>();
        for (Object item : items) saleItems.add((SaleItem) item);

        return saleItems;
    }

    public List<SaleItem> allSaleItems(String holderId) {
        List items = Persistor.fetch("select s from SaleItem s " +
                "where  s.holderId = ?1", holderId);

        List<SaleItem> saleItems = new ArrayList<>();
        for (Object item : items) saleItems.add((SaleItem) item);

        return saleItems;
    }

    public List<SaleWrapper> allWrappers(String holderId){
        List<SaleItem> saleItems = allSaleItems(holderId);
        List<SaleWrapper> sales = new ArrayList<>();
        for (SaleItem saleItem : saleItems) {
            sales.add(new SaleWrapper(saleItem));
        }
        return sales;
    }

    public List listSales(Date date) {
        return Persistor.fetch("select s from SaleItem s " +
                "where FUNC('DATE', s.date / 1000, 'unixepoch') = FUNC('DATE', ?1", date);
    }

    public boolean insert(String productId, double quantity, double amount){
        Product product = (Product) Persistor.find(Product.class,productId);
        SaleItem saleItem = new SaleItem();
        saleItem.setId(GlobalValues.generateId());
        saleItem.setAmount(amount);
        saleItem.setHolderId(GlobalValues.getCurrentSubject().getId());
        saleItem.setProductId(productId);
        saleItem.setDate(Date.from(Instant.now()));
        saleItem.setQuantity(quantity);
        saleItem.setPrice(product.getPrice());
        Persistor.insert(saleItem);
//        SaleContent sale = new SaleContent();
//        sale.setAmount(amount);
//        sale.setPrice(product.getPrice());
//        sale.setProductId(productId);
//        sale.setSaleItemId(saleItemId);
        return true;
    }

    public List<SaleItem> filteredSales(String query, List<Object> params){
        return Persistor.fetch(query, params.toArray());
    }

}
