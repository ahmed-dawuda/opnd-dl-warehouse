package com.opnd.dl.model;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.opnd.dl.domain.Payment;
import com.opnd.dl.domain.wrapper.PaymentWrapper;
import com.opnd.dl.mStuff.GlobalValues;
import com.opnd.dl.mStuff.LogUtil;
import com.opnd.dl.mStuff.Persistor;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class PaymentUtil {

    private String holderId = GlobalValues.getCurrentSubject().getId();
    private String key = GlobalValues.getCurrentSubject().getKey();
    private final String BASE_URL = GlobalValues.getBaseUrl();

    @SuppressWarnings("unchecked")
    public void postUnsynced() throws UnirestException {
        List<String> l = Persistor.fetch("Select p.id from Payment p where p.synced = ?1", false);
//        LogUtil.log("PAYMENT - "+l.toString());
        for (String id : l) {
            if(postPayment(id)){
//                LogUtil.log("postUnsynced()- payment synced");
                Payment c = localFetchPayment(id);
                Persistor.begin();
                c.setSynced(true);
                Persistor.commit();
            }
        }
    }

    private Payment localFetchPayment(String id) {
        return (Payment) Persistor.find(Payment.class, id);
    }

    private boolean postPayment(String id) throws UnirestException {
//        LogUtil.log("PAYMENT - POST - "+id);
        String url = BASE_URL + "payments";
        Payment item = localFetchPayment(id);

        Map params = new ObjectMapper().convertValue(item, Map.class);
//        LogUtil.log("PAYMENT PARAMS - "+params.toString());
        final HttpResponse<JsonNode> response = Unirest.post(url)
                .header("Authorization", key)
                .header("Accept", "application/json")
                .fields(params)
                .asJson();

        JSONObject object = response.getBody().getObject();
//        LogUtil.log(object.toString(2));
        return object.has("status") && object.getBoolean("status");
    }


    public List<Payment> payments(){
        return Persistor.fetch("Select p from Payment p where p.holderId = ?1", GlobalValues.getCurrentSubject().getId());
    }

    public List<PaymentWrapper> wrappedPayments(){
        List<Payment> allPayments = payments();
        List<PaymentWrapper> paymentWrappers = new ArrayList<>();
        for (Payment payment : allPayments) {
            paymentWrappers.add(new PaymentWrapper(payment));
        }
        return paymentWrappers;
    }
}
