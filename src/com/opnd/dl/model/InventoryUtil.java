package com.opnd.dl.model;

import com.google.gson.Gson;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.opnd.dl.domain.InventoryItem;
import com.opnd.dl.mStuff.GlobalValues;
import com.opnd.dl.mStuff.LogUtil;
import com.opnd.dl.mStuff.Persistor;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONObject;
import sun.rmi.runtime.Log;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class InventoryUtil {

    private String holderId = GlobalValues.getCurrentSubject().getId();
    private String key = GlobalValues.getCurrentSubject().getKey();
    private final String BASE_URL = GlobalValues.getBaseUrl();

    protected class IdHolderId {
        private String id;
        private String holderId;

        public IdHolderId(Object[] o) {
            id = (String) o[0];
            holderId = (String) o[1];
        }

        public boolean isInList(List<IdHolderId> list) {

            for (IdHolderId idHolderId : list)
                if (id.equalsIgnoreCase(idHolderId.getId()) && holderId.equalsIgnoreCase(idHolderId.getHolderId()))
                    return true;

            return false;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getHolderId() {
            return holderId;
        }

        public void setHolderId(String holderId) {
            this.holderId = holderId;
        }

        @Override
        public String toString() {
            return "id="+id+", holderId="+holderId;
        }
    }

    public List<InventoryItem> inventories(){
        return Persistor.fetch("select i from InventoryItem i where i.holderId = ?1",GlobalValues.getCurrentSubject().getId());
    }

    private List<IdHolderId> localFetchIds() {
        List<IdHolderId> ids = new ArrayList<>();
        List l = Persistor.fetch("select i.id, i.holderId from InventoryItem i where i.holderId = ?1", GlobalValues.getCurrentSubject().getId());

        for (Object o : l) {
            ids.add(new IdHolderId((Object[]) o));
        }

        return ids;
    }

    public InventoryItem inventoryItem(String productId){
        List<InventoryItem> items =  Persistor.fetch(
                "select i from InventoryItem i where i.holderId = ?1 and i.productId = ?2",
                GlobalValues.getCurrentSubject().getId(),productId);
        if(items.size() > 0){
            return items.get(0);
        }
        return null;
    }

    private InventoryItem localFetchInventoryItem(String id) {
        return (InventoryItem) Persistor.find(InventoryItem.class, id);
    }

    private List<IdHolderId> remoteFetchIds() throws UnirestException {
        String url = BASE_URL + "inventoryItems/idsOnly";
        List<IdHolderId> ids = new ArrayList<>();

        final HttpResponse<JsonNode> response = Unirest.get(url)
                .header("Authorization", key)
                .header("Accept", "application/json")
                .asJson();

        JSONObject object = response.getBody().getObject();
        if (object.has("status") && object.getBoolean("status")) {
            JSONArray idListJson = object.getJSONArray("message");
//            LogUtil.log(object.toString(2));
            for (int i = 0; i < idListJson.length(); i++) {
//                ids.add(new Gson().fromJson(idListJson.toString(), IdHolderId.class));
//                LogUtil.log(idListJson.getJSONObject(i).toString());
                ids.add(new IdHolderId(new Object[]{idListJson.getJSONObject(i).getString("id"), idListJson.getJSONObject(i).getString("holderId")}));
            }
//            LogUtil.log(object.getJSONArray("message").toString());

        }
//        LogUtil.log(object.toString(2));
//        LogUtil.log(ids.toString());
        return ids;
    }

    private InventoryItem remoteFetchInventoryItem(IdHolderId id) throws UnirestException {
        String url = BASE_URL + "inventoryItems/" + id.getId() + "/" + id.getHolderId();

        final HttpResponse<JsonNode> response = Unirest.get(url)
                .header("Authorization", key)
                .header("Accept", "application/json")
                .asJson();

        JSONObject object = response.getBody().getObject();
        if (object.has("status") && object.getBoolean("status")) {
            JSONObject itemJson = object.getJSONObject("message");
            InventoryItem item = new InventoryItem();
            item.setId(itemJson.getString("id"));
            item.setSynced(true);
            item.setQuantity(itemJson.getInt("quantity"));
            item.setProductId(itemJson.getString("productId"));
            item.setHolderId(itemJson.getString("holderId"));
            item.setDate(new Date(itemJson.getLong("date")));
//            Gson gson = new Gson();
//            return gson.fromJson(itemJson.toString(), InventoryItem.class);
            return item;
        }

        return null;
    }

    private boolean downloadInventoryItem(IdHolderId id) throws UnirestException {
        InventoryItem item = remoteFetchInventoryItem(id);
//        LogUtil.log(item.toString());
//        return false;
        if(item != null){
            item.setSynced(true);
            try {
                Persistor.insert(item);
                return true;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return false;
    }

    private boolean postInventoryItem(IdHolderId id) throws UnirestException {
        String url = BASE_URL + "inventoryItems";
        InventoryItem item = localFetchInventoryItem(id.getId());
        Map params = new ObjectMapper().convertValue(item, Map.class);

        final HttpResponse<JsonNode> response = Unirest.post(url)
                .header("Authorization", key)
                .header("Accept", "application/json")
                .fields(params)
                .asJson();

        JSONObject object = response.getBody().getObject();
        LogUtil.log(object.toString(2));
        LogUtil.log((object.has("status") && object.getBoolean("status"))+"");
        return object.has("status") && object.getBoolean("status");
    }

    public void sync() throws UnirestException {
        List<IdHolderId> localList = localFetchIds();
        List<IdHolderId> remoteList = remoteFetchIds();

        List<IdHolderId> localOnly = new ArrayList<>();
        List<IdHolderId> remoteOnly = new ArrayList<>();

        for (IdHolderId number : localList) {
            if (!number.isInList(remoteList))
                localOnly.add(number);
        }

        for (IdHolderId number : remoteList) {
            if (!number.isInList(localList))
                remoteOnly.add(number);
        }

        for (IdHolderId number : localOnly) {
            postInventoryItem(number);
        }

        for (IdHolderId number : remoteOnly) {
            downloadInventoryItem(number);
        }
    }

    @SuppressWarnings("unchecked")
    public void postUnsynced() throws UnirestException{
        List l = Persistor.fetch("select i.id, i.holderId from InventoryItem i where i.synced = ?1 and i.holderId = ?2",
                false, GlobalValues.getCurrentSubject().getId());
//        LogUtil.log("unsynced: "+ l);

        for (Object id : l) {
            IdHolderId idH = new IdHolderId((Object[]) id);
            if(postInventoryItem(idH)){
                InventoryItem i = localFetchInventoryItem(idH.getId());
                Persistor.begin();
                i.setSynced(true);
                Persistor.commit();
            }
        }
//        LogUtil.log(l.toString());
    }



    public List<InventoryItem> listInventory(String holderId, Date date) {
        List inventory = Persistor.fetch("select i from InventoryItem i where " +
                "i.holderId = ? and FUNC('DATE', i.date) = FUNC('DATE', ?)", holderId, date);

        List<InventoryItem> items = new ArrayList<>();
        for (Object o : inventory) items.add((InventoryItem) o);

        return items;
    }

    public List<InventoryItem> listInventory(Date date) {
        List inventory = Persistor.fetch("select i.id from InventoryItem i where " +
                "FUNC('DATE', i.date) = FUNC('DATE', ?)", date);

        List<InventoryItem> items = new ArrayList<>();
        for (Object o : inventory) items.add((InventoryItem) o);

        return items;
    }

    public List<InventoryItem> listInventory(String holderId) {
        List inventory = Persistor.fetch("select i from InventoryItem i where " +
                "i.holderId = ?", holderId);

        List<InventoryItem> items = new ArrayList<>();
        for (Object o : inventory) items.add((InventoryItem) o);

        return items;
    }

    public void insert(String productId, double quantity){
        InventoryItem inventoryItem = new InventoryItem();
        inventoryItem.setId(GlobalValues.generateId());
        inventoryItem.setHolderId(GlobalValues.getCurrentSubject().getId());
        inventoryItem.setProductId(productId);
        inventoryItem.setQuantity(quantity);
        inventoryItem.setDate(Date.from(Instant.now()));
        Persistor.insert(inventoryItem);
    }

//    public boolean putInventoryItem(Number id) throws UnirestException {
//        String url = BASE_URL + "inventoryItems/"+id+"/"+holderId;
//        InventoryItem item = localFetchInventoryItem(id);
//        Map params = new ObjectMapper().convertValue(item, Map.class);
//
//        final HttpResponse<JsonNode> response = Unirest.put(url)
//                .header("Authorization", key)
//                .header("Accept", "application/json")
//                .fields(params)
//                .asJson();
//
//        JSONObject object = response.getBody().getObject();
//        LogUtil.log(object.toString(2));
//        return object.has("status") && object.getBoolean("status");
//    }

//    public void putUnupdated() throws UnirestException {
//        List<Number> l = Persistor.fetch("select i.id from InventoryItem i where i.updated = ?1", false);
//            for(Number id: l){
//                if(putInventoryItem(id)){
//                    InventoryItem i = localFetchInventoryItem(id);
//                    Persistor.begin();
//                    i.setUpdated(true);
//                    Persistor.commit();
//                }
//            }
//    }

    public boolean putInventoryItem(String id) throws UnirestException {
        String url = BASE_URL + "inventoryItems/"+id+"/"+holderId;
//        LogUtil.log(url);
        InventoryItem item = localFetchInventoryItem(id);
        Map params = new ObjectMapper().convertValue(item, Map.class);

        final HttpResponse<JsonNode> response = Unirest.put(url)
                .header("Authorization", key)
                .header("Accept", "application/json")
                .fields(params)
                .asJson();

        JSONObject object = response.getBody().getObject();
//        LogUtil.log(object.toString(2));
        return object.has("status") && object.getBoolean("status");
    }

    public void putUpdated() throws UnirestException {
        List<String> ids = Persistor.fetch("Select i.id from InventoryItem i where i.updated = ?1", false);
//        LogUtil.log(ids.toString());
        for (String id : ids) {
            if(putInventoryItem(id)){
                InventoryItem item = localFetchInventoryItem(id);
                Persistor.begin();
                item.setUpdated(true);
                Persistor.commit();
            }
        }
    }

    public List<InventoryItem> filteredInventories(String query, List<Object> params){
        return Persistor.fetch(query, params.toArray());
    }
}
