package com.opnd.dl.model;

import com.google.gson.Gson;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.opnd.dl.domain.NewStockItem;
import com.opnd.dl.domain.Requisition;
import com.opnd.dl.domain.TransferItem;
import com.opnd.dl.domain.wrapper.TransferRequestWrapper;
import com.opnd.dl.mStuff.GlobalValues;
import com.opnd.dl.mStuff.LogUtil;
import com.opnd.dl.mStuff.Persistor;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class TransferUtil {


    private String holderId = GlobalValues.getCurrentSubject().getId();
    private String key = GlobalValues.getCurrentSubject().getKey();
    private final String BASE_URL = GlobalValues.getBaseUrl();
    RequisitionUtil requisitionUtil = new RequisitionUtil();

    private List<String> fetchRequisitionIds(){
        List<String> ids = Persistor.fetch("Select r.id from Requisition r where r.deleted = ?1 and r.tIDownloaded = ?2", false,false);
        return ids;
    }

    private List<TransferItem> remoteFetchTransferItems(String id) throws UnirestException {
        String url = BASE_URL + "transferItems?requisitionId="+id;
        List<TransferItem> items = new ArrayList<>();

        final HttpResponse<JsonNode> response = Unirest.get(url)
                .header("Authorization", key)
                .header("Accept", "application/json")
                .asJson();

        JSONObject object = response.getBody().getObject();
        if (object.has("status") && object.getBoolean("status")) {
            JSONArray idListJson = object.getJSONArray("message");
//            LogUtil.log(object.toString(2));
            for (int i = 0; i < idListJson.length(); i++) {
                items.add(new Gson().fromJson(idListJson.get(i).toString(), TransferItem.class));
            }
            Requisition requisition = requisitionUtil.localFetchRequisition(id);
            Persistor.begin();
            requisition.settIDownloaded(true);
            Persistor.commit();
        }
        return items;
    }


    private TransferItem localFetchTransferItem(String id) {
        return (TransferItem) Persistor.find(TransferItem.class, id);
    }



    public void sync() throws UnirestException {
        List<String> ids = fetchRequisitionIds();
        for (String id : ids) {
            List<TransferItem> items = remoteFetchTransferItems(id);
            for (TransferItem item : items) {
                Persistor.insert(item);
            }

        }
    }



    public static void insert(NewStockItem newStockItem){
        TransferItem transferItem = new TransferItem();
//        transferItem.setDate(new Date());
        transferItem.setQuantity(newStockItem.getQuantity());
//        transferItem.setDirection(0);
//        transferItem.setHolderId(342345);
        transferItem.setProductId(newStockItem.getProduct().getId());
        Persistor.insert(transferItem);
    }

    public List<Requisition> requisitions(){
        return Persistor.fetch("select r from Requisition r where r.status = ?1 and r.targetId = ?2",
                Requisition.RequisitionStatus.APPROVED, GlobalValues.getCurrentSubject().getId());
    }

    public List<TransferRequestWrapper> wrappedRequisitions(){
        List<Requisition> requisitions = requisitions();
        List<TransferRequestWrapper> transferRequestWrappers = new ArrayList<>();
        for (Requisition requisition : requisitions) {
            transferRequestWrappers.add(new TransferRequestWrapper(requisition));
        }
        return transferRequestWrappers;
    }

}
