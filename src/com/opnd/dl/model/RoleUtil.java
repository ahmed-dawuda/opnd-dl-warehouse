package com.opnd.dl.model;

import com.google.gson.Gson;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.opnd.dl.domain.Role;
import com.opnd.dl.mStuff.GlobalValues;
import com.opnd.dl.mStuff.LogUtil;
import com.opnd.dl.mStuff.Persistor;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class RoleUtil {

//    private String holderId = GlobalValues.getCurrentSubject().getEmail();
//    private String key = GlobalValues.getCurrentSubject().getKey();
    private final String BASE_URL = GlobalValues.getBaseUrl();


    private List<String> localFetchIds() {
        List<String> ids = new ArrayList<>();
        List l = Persistor.fetch("select r.id from Role r");

        for (Object o : l) {
            ids.add((String) o);
        }

        return ids;
    }

    private Role localFetchRole(String id) {
        return (Role) Persistor.find(Role.class, id);
    }

    private List<String> remoteFetchIds() throws UnirestException {
        String url = BASE_URL + "roles/idsOnly";
        List<String> ids = new ArrayList<>();

        final HttpResponse<JsonNode> response = Unirest.get(url)
                .header("Authorization", GlobalValues.getCurrentSubject().getKey())
                .header("Accept", "application/json")
                .asJson();

        JSONObject object = response.getBody().getObject();
        if (object.has("status") && object.getBoolean("status")) {
            JSONArray idListJson = object.getJSONArray("message");
            for (int i = 0; i < idListJson.length(); i++) {
                ids.add(idListJson.getString(i));
            }
        }

        return ids;
    }

    private Role remoteFetchRole(String id) throws UnirestException {
        String url = BASE_URL + "roles/" + id;

        final HttpResponse<JsonNode> response = Unirest.get(url)
                .header("Authorization", GlobalValues.getCurrentSubject().getKey())
                .header("Accept", "application/json")
                .asJson();

        JSONObject object = response.getBody().getObject();
        if (object.has("status") && object.getBoolean("status")) {
            JSONObject itemJson = object.getJSONObject("message");
            Gson gson = new Gson();
            return gson.fromJson(itemJson.toString(), Role.class);
        }

        return null;
    }

    private boolean downloadRole(String id) throws UnirestException {
        Role item = remoteFetchRole(id);
//        LogUtil.log(item.toString());

//        if (localFetchRole(id) == null) return true;

        if(item != null){
            try {
                item.setSynced(true);
                Persistor.insert(item);
                return true;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    private boolean postRole(String id) throws UnirestException {
        String url = BASE_URL + "roles";
        Role item = localFetchRole(id);
        Map params = new ObjectMapper().convertValue(item, Map.class);

        final HttpResponse<JsonNode> response = Unirest.post(url)
                .header("Authorization", GlobalValues.getCurrentSubject().getKey())
                .header("Accept", "application/json")
                .fields(params)
                .asJson();

        JSONObject object = response.getBody().getObject();
        return object.has("status") && object.getBoolean("status");
    }

    public void sync() throws UnirestException {
        List<String> localList = localFetchIds();
        List<String> remoteList = remoteFetchIds();

//        LogUtil.log("local list ids for roles " + localList);
//        LogUtil.log("remote list ids for roles " + remoteList);

        List<String> localOnly = new ArrayList<>();
        List<String> remoteOnly = new ArrayList<>();

        for (String number : localList) {
            if (!remoteList.contains(number))
                localOnly.add(number);
        }

//        LogUtil.log("role localOnly "+ localOnly);

        for (String number : remoteList) {
            if (!localList.contains(number))
                remoteOnly.add(number);
        }

//        LogUtil.log("role remoteOnly "+ remoteOnly);

        for (String number : localOnly) {
            postRole(number);
        }

        for (String number : remoteOnly) {
            downloadRole(number);
        }

//        LogUtil.log("ROLE UTIL IS CALLED");
    }

    @SuppressWarnings("unchecked")
    private void postUnsynced() throws UnirestException {
        List<String> l = Persistor.fetch("select r.id from Role r where r.synced = ?1", false);
        for (String id : l) {
            if(postRole(id)){
                Role c = localFetchRole(id);
                Persistor.begin();
                c.setSynced(true);
                Persistor.commit();
            }
        }
    }


    public void insert(String description){
        Role role = new Role();
        role.setDescription(description);
        Persistor.insert(role);
        new Thread(() -> {
            try {
                postUnsynced();
            } catch (UnirestException e) {
                e.printStackTrace();
            }
        }).start();
    }
    public List<Role> allRoles(){
        return Persistor.fetch("select r from Role r where r.description != ?1", "Admin");
    }

    public Role find(String id){
        return (Role) Persistor.find(Role.class, id);
    }
}
