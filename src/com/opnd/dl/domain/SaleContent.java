package com.opnd.dl.domain;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class SaleContent {

    @Id
    private long id;
    private long saleItemId;
    private long productId;
    private double quantity;
    private double price;
    private double amount;
    private boolean synced = false;
}
