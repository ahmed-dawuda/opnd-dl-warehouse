package com.opnd.dl.domain.wrapper;

import com.opnd.dl.domain.Requisition;
import com.opnd.dl.domain.Subject;

import java.util.Date;

public class PaymentsReportWrapper {
    private Date date;
    private String location;
    private Subject paidBy;
    private Double amount;
    private Requisition.RequisitionStatus status;
}
