package com.opnd.dl.domain.wrapper;

import com.opnd.dl.domain.Category;
import com.opnd.dl.domain.Product;
import com.opnd.dl.domain.SaleItem;
import com.opnd.dl.mStuff.Persistor;

import java.util.Date;

public class SaleWrapper {
    private Date date;
    private String Location;
    private Product product;
    private Category category;
    private Double quantity;
    private Double price;
    private Double amount;

    public SaleWrapper(SaleItem saleItem){
        this.date = saleItem.getDate();
        this.product = (Product) Persistor.find(Product.class, saleItem.getProductId());
        this.category = (Category) Persistor.find(Category.class, product.getCategoryId());
        this.quantity = saleItem.getQuantity();
        this.amount = saleItem.getAmount();
        this.price = this.product.getPrice();

    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getLocation() {
        return Location;
    }

    public void setLocation(String location) {
        Location = location;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Double getQuantity() {
        return quantity;
    }

    public void setQuantity(Double quantity) {
        this.quantity = quantity;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }
}
