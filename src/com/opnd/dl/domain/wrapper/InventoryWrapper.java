package com.opnd.dl.domain.wrapper;

import com.opnd.dl.domain.Category;
import com.opnd.dl.domain.InventoryItem;
import com.opnd.dl.domain.Product;
import com.opnd.dl.mStuff.Persistor;

import java.util.Date;

public class InventoryWrapper {
    private String inventoryItemId;
    private double unitprice;
    private double quantity;
    private double amount;
    private Product product;
    private Category category;
    private Date date;
    private String location;

    public InventoryWrapper(InventoryItem inventoryItem){
        this.inventoryItemId = inventoryItem.getId();
        this.quantity = inventoryItem.getQuantity();
        this.date = inventoryItem.getDate();
        this.product = (Product) Persistor.find(Product.class, inventoryItem.getProductId());
        this.category = (Category) Persistor.find(Category.class, product.getCategoryId());
        this.amount = this.product.getPrice() * this.quantity;
        this.unitprice = this.product.getPrice();

    }

    public String getInventoryItemId() {
        return inventoryItemId;
    }

    public void setInventoryItemId(String inventoryItemId) {
        this.inventoryItemId = inventoryItemId;
    }

    public double getUnitprice() {
        return unitprice;
    }

    public void setUnitprice(double unitprice) {
        this.unitprice = unitprice;
    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
