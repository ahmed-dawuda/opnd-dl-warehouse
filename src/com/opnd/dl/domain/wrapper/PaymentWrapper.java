package com.opnd.dl.domain.wrapper;

import com.opnd.dl.domain.Payment;

import java.util.Date;

public class PaymentWrapper {
    private Date date;
    private Double amount;

    public PaymentWrapper(Payment payment){
        date = payment.getDate();
        amount = payment.getAmount();
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }
}
