package com.opnd.dl.domain;

import javax.persistence.*;

@Entity
public class Role {

    @Id
//    @GeneratedValue(strategy = GenerationType.AUTO)
    private String id;
    private String description;
    private boolean deleted;
    private boolean synced = false;

    public boolean isSynced() {
        return synced;
    }

    public void setSynced(boolean synced) {
        this.synced = synced;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
