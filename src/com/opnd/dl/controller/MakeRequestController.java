package com.opnd.dl.controller;

import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import com.opnd.dl.domain.NewStockItem;
import com.opnd.dl.domain.Product;
import com.opnd.dl.domain.Requisition;
import com.opnd.dl.domain.TransferItem;
import com.opnd.dl.mStuff.GlobalValues;
import com.opnd.dl.mStuff.Persistor;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.util.StringConverter;

import java.text.DecimalFormat;
import java.time.Instant;
import java.util.Date;
import java.util.List;

public class MakeRequestController {

    @FXML private JFXComboBox<Product> product;
    @FXML private JFXTextField amount;
    @FXML private JFXTextField quantity;

    @FXML private TableView<NewStockItem> requestTable;
    @FXML private TableColumn<NewStockItem, Product> productCol;
    @FXML private TableColumn<NewStockItem, Double> quantityCol;
    @FXML private TableColumn<NewStockItem, Double> amountCol;
    @FXML private TableColumn<NewStockItem, Product> deleteCol;
    @FXML private Label message;

    private double q = 0;
    private double amt = 0;

    public void initialize(){
        productCol.setCellValueFactory(new PropertyValueFactory<>("product"));
        quantityCol.setCellValueFactory(new PropertyValueFactory<>("quantity"));
        amountCol.setCellValueFactory(new PropertyValueFactory<>("amount"));
        deleteCol.setCellValueFactory(new PropertyValueFactory<>("product"));

        productCol.setCellFactory(param -> new TableCell<NewStockItem, Product>(){
            @Override
            protected void updateItem(Product item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : item.getDescription());
            }
        });

        DecimalFormat df = new DecimalFormat("#,###,###.##");

        quantityCol.setCellFactory(param -> new TableCell<NewStockItem, Double>(){
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : df.format(item));
            }
        });

        amountCol.setCellFactory(param -> new TableCell<NewStockItem, Double>(){
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : df.format(item));
            }
        });

        deleteCol.setCellFactory(param -> new TableCell<NewStockItem, Product>(){
            @Override
            protected void updateItem(Product item, boolean empty) {
                super.updateItem(item, empty);
                Hyperlink deleteLink = new Hyperlink("DELETE");
                deleteLink.setOnAction(e -> delete(item));
                deleteLink.setStyle("-fx-alignment: baseline-center");
                deleteLink.setStyle("-fx-text-fill: #E74C3C");
                setGraphic(item == null || empty ? null : deleteLink);
            }
        });

        product.setConverter(new StringConverter<Product>() {
            @Override
            public String toString(Product object) {
                return object.getDescription();
            }

            @Override
            public Product fromString(String string) {
                return null;
            }
        });

        product.setCellFactory(param -> new ListCell<Product>(){
            @Override
            protected void updateItem(Product item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : item.getDescription());
            }
        });

        product.setItems(FXCollections.observableArrayList(Persistor.fetchAll("Product")));

        quantity.textProperty().addListener((observable, oldValue, newValue) -> {
            newValue = newValue.isEmpty() ? "0" : newValue;
            try{
                q = Double.parseDouble(newValue);
                amt = product.getSelectionModel().getSelectedItem() == null ? 0 : q * product.getSelectionModel().getSelectedItem().getPrice();
                amount.setText(amt == 0? "" : amt+"");
            }catch (Exception ex){
                quantity.setText(oldValue);
            }
        });

        requestTable.setItems(FXCollections.observableArrayList());
    }

    public void delete(Product product){
        NewStockItem delete = null;
        for (NewStockItem item : requestTable.getItems()) {
            if(item.getProduct().getId() == product.getId()){
                delete = item;
            }
        }
        requestTable.getItems().remove(delete);
    }

    public void add(){
        NewStockItem newStockItem = new NewStockItem();
        newStockItem.setQuantity(q);
        newStockItem.setProduct(product.getSelectionModel().getSelectedItem());
        newStockItem.setAmount(amt);
        if(!requestHas(newStockItem)){
            requestTable.getItems().add(newStockItem);
            message.setText("Item added to request");
            cancel();
        }else{
            message.setText("You've already added the item\nDelete the entry in the table\nbefore you can add again");
        }
    }

    public void submit(){
        List<NewStockItem> items = requestTable.getItems();
        Requisition requisition = new Requisition();
        requisition.setStatus(Requisition.RequisitionStatus.PENDING);
        requisition.setDate(Date.from(Instant.now()));
        requisition.setHolderId(GlobalValues.getCurrentSubject().getId());
        Persistor.insert(requisition);

        for (NewStockItem item : items) {
            TransferItem transferItem = new TransferItem();
            transferItem.setProductId(item.getProduct().getId());
            transferItem.setQuantity(item.getQuantity());
            transferItem.setRequisitionId(requisition.getId());
            Persistor.insert(transferItem);
        }
        requestTable.getItems().clear();
        message.setText("REQUEST SENT FOR REVIEW");
    }

    public void cancel(){
        product.getSelectionModel().clearSelection();
        quantity.clear();
        amount.clear();
    }

    private boolean requestHas(NewStockItem item){
        for (NewStockItem newStockItem : requestTable.getItems()) {
            if(newStockItem.getProduct().getId() == item.getProduct().getId()){
                return true;
            }
        }
        return false;
    }

}
