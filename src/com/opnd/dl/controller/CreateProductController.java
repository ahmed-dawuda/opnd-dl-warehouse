package com.opnd.dl.controller;

import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import com.opnd.dl.domain.Category;
import com.opnd.dl.mStuff.LogUtil;
import com.opnd.dl.model.ProductUtil;
import javafx.application.Platform;
import javafx.fxml.FXML;

public class CreateProductController {

    @FXML private JFXTextField description;
    @FXML private JFXComboBox<Category> category;
    @FXML private JFXTextField price;

    private double priceValue = 0;

    @FXML
    public void initialize() {
        price.textProperty().addListener((observable, oldValue, newValue) -> {
            newValue = newValue.isEmpty() ? "0" : newValue;

            if (!newValue.matches("^[0-9]*\\.?[0-9]*$")) price.setText(oldValue);
            else priceValue = Double.parseDouble(newValue);
        });
    }

    @FXML
    private void create() {
        System.out.println(priceValue);
        String desc = description.getText();
        Category cat = category.getSelectionModel().getSelectedItem();
        if (desc.isEmpty()) {
            LogUtil.log("You have not entered the description");
            return;
        }

        if (priceValue == 0) {
            LogUtil.log("You have not entered the price");
            return;
        }

        if (cat == null) {
            LogUtil.log("There is no category");
            return;
        }

        ProductUtil util = new ProductUtil();
        util.createProduct(desc, priceValue, cat.getId());
        LogUtil.log("Product created");
    }

    @FXML
    public void close() {
        Platform.exit();
    }
}
