package com.opnd.dl.controller;

import com.jfoenix.controls.JFXTextField;
import com.opnd.dl.domain.Payment;
import com.opnd.dl.mStuff.GlobalValues;
import com.opnd.dl.mStuff.Persistor;
import javafx.fxml.FXML;
import javafx.scene.control.Label;

import java.time.Instant;
import java.util.Date;

public class MakePaymentController {

    @FXML private JFXTextField amount;
    @FXML private Label message;
    private double amnt = 0;

    public void initialize(){
        amount.textProperty().addListener((observable, oldValue, newValue) -> {
            newValue = newValue.isEmpty() ? "0" : newValue;
            try{
                amnt = Double.parseDouble(newValue);
            }catch(Exception ex){
                amount.setText(oldValue);
                return;
            }
        });
    }

    public void add(){
        if(amnt != 0){
            Payment payment = new Payment();
            payment.setId(GlobalValues.generateId());
            payment.setAmount(amnt);
            payment.setDate(Date.from(Instant.now()));
            payment.setHolderId(GlobalValues.getCurrentSubject().getId());
            Persistor.insert(payment);
            cancel();
        }
    }

    public void cancel(){
        amount.clear();
    }

}
