package com.opnd.dl.controller;

import com.jfoenix.controls.JFXCheckBox;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import com.opnd.dl.domain.*;
import com.opnd.dl.domain.wrapper.SaleWrapper;
import com.opnd.dl.mStuff.GlobalValues;
import com.opnd.dl.mStuff.LogUtil;
import com.opnd.dl.mStuff.Persistor;
import com.opnd.dl.model.InventoryUtil;
import com.opnd.dl.model.ProductUtil;
import com.opnd.dl.model.SaleUtil;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class SaleController {

    @FXML private JFXCheckBox startDateCB;
    @FXML private DatePicker startDateDP;
    @FXML private DatePicker endDateDP;
    @FXML private JFXCheckBox endDateCB;
//    @FXML private JFXCheckBox locationCB;
//    @FXML private JFXComboBox locationCMB;
//    @FXML private JFXCheckBox salespersonCB;
//    @FXML private JFXComboBox salespersonCMB;

    //form fields
    @FXML private JFXTextField amount;
    @FXML private JFXTextField quantity;
    @FXML private JFXComboBox<Product> product;

    //table
    @FXML private TableView<NewStockItem> stockTable;
    @FXML private TableColumn<NewStockItem, Product> productCol;
    @FXML private TableColumn<NewStockItem, Double> amountCol;
    @FXML private TableColumn<NewStockItem, Double> quantityCol;
    @FXML private TableColumn<NewStockItem, Product> deleteCol;

    @FXML private TableView<SaleWrapper> saleTable;
    @FXML private TableColumn<SaleWrapper, Date> dateCol;
    @FXML private TableColumn<SaleWrapper, String> locationCol;
    @FXML private TableColumn<SaleWrapper, Product> saleProductCol;
    @FXML private TableColumn<SaleWrapper, Category> categoryCol;
    @FXML private TableColumn<SaleWrapper, Double> saleQuantityCol;
    @FXML private TableColumn<SaleWrapper, Double> saleAmountCol;
    @FXML private TableColumn<SaleWrapper, Double> unitPriceCol;
    @FXML private Label username;
    @FXML private Label role;

    private InventoryUtil inventoryUtil = new InventoryUtil();



    private double qty = 0;
    private double amt = 0;
    private Product p;

    private SaleUtil saleUtil;

    //initializing
    public void initialize(){
        this.saleUtil = new SaleUtil();
        if(GlobalValues.getCurrentSubject() != null ){
            this.username.setText(GlobalValues.getCurrentSubject().getFullname());
            this.role.setText("Warehouse");
        }

        startDateDP.disableProperty().bind(startDateCB.selectedProperty().not());
        endDateDP.disableProperty().bind(endDateCB.selectedProperty().not());
//        locationCMB.disableProperty().bind(locationCB.selectedProperty().not());
//        salespersonCMB.disableProperty().bind(salespersonCB.selectedProperty().not());

        productCol.setCellValueFactory(new PropertyValueFactory<>("product"));
        amountCol.setCellValueFactory(new PropertyValueFactory<>("amount"));
        quantityCol.setCellValueFactory(new PropertyValueFactory<>("quantity"));
        deleteCol.setCellValueFactory(new PropertyValueFactory<>("product"));

        DecimalFormat df = new DecimalFormat("#,###,###.##");

        amountCol.setCellFactory(param -> new TableCell<NewStockItem, Double>(){
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText((item == null || empty) ? null : df.format(item));
            }
        });

        quantityCol.setCellFactory(param -> new TableCell<NewStockItem, Double>(){
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText((item == null || empty) ? null : df.format(item));
            }
        });

        productCol.setCellFactory(param -> new TableCell<NewStockItem, Product>() {
            @Override
            protected void updateItem(Product item, boolean empty) {
                super.updateItem(item, empty);
                setText((item == null || empty) ? null : item.getDescription());
            }
        });

        deleteCol.setCellFactory(param -> new TableCell<NewStockItem, Product>(){
            @Override
            protected void updateItem(Product item, boolean empty) {
                super.updateItem(item, empty);
                Hyperlink deletLink =  new Hyperlink("DELETE");
                deletLink.setOnAction(e -> deleteRow(item));
                deletLink.setStyle("-fx-alignment: baseline-center");
                setGraphic(item == null || empty ? null : deletLink);
            }
        });

        product.setCellFactory(param -> new ListCell<Product>(){
            @Override
            protected void updateItem(Product item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : item.getDescription());
            }
        });


        quantity.textProperty().addListener((observable, oldValue, newValue) -> {
            newValue = newValue.isEmpty() ? "0" : newValue;

            if (!newValue.matches("^[+-]?\\d+$")) {
                quantity.setText(oldValue);
                return;
            }

            qty = Double.parseDouble(newValue);
            amt = qty * p.getPrice();
            amount.setText(df.format(amt));
        });

        amount.setEditable(false);
        product.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> p = newValue);

        ProductUtil util = new ProductUtil();
        product.setItems(FXCollections.observableArrayList(util.allProducts()));
        stockTable.setItems(FXCollections.observableArrayList());

        dateCol.setCellValueFactory(new PropertyValueFactory<>("date"));
        unitPriceCol.setCellValueFactory(new PropertyValueFactory<>("price"));
        categoryCol.setCellValueFactory(new PropertyValueFactory<>("category"));
        saleAmountCol.setCellValueFactory(new PropertyValueFactory<>("amount"));
        saleProductCol.setCellValueFactory(new PropertyValueFactory<>("product"));
        saleQuantityCol.setCellValueFactory(new PropertyValueFactory<>("quantity"));



        dateCol.setCellFactory(param -> new TableCell<SaleWrapper, Date>(){
            @Override
            protected void updateItem(Date item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : DateFormat.getDateInstance().format(item));
            }
        });

        unitPriceCol.setCellFactory(param -> new TableCell<SaleWrapper, Double>(){
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : df.format(item));
            }
        });

        categoryCol.setCellFactory(param -> new TableCell<SaleWrapper, Category>(){
            @Override
            protected void updateItem(Category item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : item.getDescription());
            }
        });

        saleAmountCol.setCellFactory(param -> new TableCell<SaleWrapper, Double>(){
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : df.format(item));
            }
        });

        saleQuantityCol.setCellFactory(param -> new TableCell<SaleWrapper, Double>(){
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : df.format(item));
            }
        });

        saleProductCol.setCellFactory(param -> new TableCell<SaleWrapper, Product>(){
            @Override
            protected void updateItem(Product item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : item.getDescription());
            }
        });

        saleTable.setItems(FXCollections.observableArrayList(saleUtil.allWrappers(GlobalValues.getCurrentSubject().getId())));
    }


    @FXML private void deleteRow(Product item){
        for (NewStockItem newStockItem : stockTable.getItems()) {
            if(newStockItem.getProduct().getId() == item.getId()){
                stockTable.getItems().remove(newStockItem);
            }
        }
    }

    @FXML private void add(){
        if(amt != 0 && qty != 0 && p != null){
            NewStockItem item = new NewStockItem();
            item.setAmount(amt);
            item.setProduct(p);
            item.setQuantity(qty);
            stockTable.getItems().add(item);
            this.quantity.clear();
            this.amount.clear();

        }
    }

    @FXML private void cancel(){
        stockTable.getItems().clear();
    }

    @FXML private void finish(){
        Date date = Date.from(Instant.now());
        List<NewStockItem> list = stockTable.getItems();
//        LogUtil.log(list.toString()+ list.size());

        for (int i = 0; i < list.size(); i++) {

            NewStockItem newStockItem = list.get(i);

            InventoryItem item = inventoryUtil.inventoryItem(newStockItem.getProduct().getId());

            if(newStockItem.getQuantity() < item.getQuantity()){
                saleUtil.insert(newStockItem.getProduct().getId(), newStockItem.getQuantity(), newStockItem.getAmount());
                Persistor.begin();
                item.setQuantity(item.getQuantity() - newStockItem.getQuantity());
                item.setUpdated(false);
                Persistor.commit();
//                cancel();
            }else{
                System.out.println("You don't have enough of this item to sell");
            }
        }
        cancel();
        saleTable.setItems(FXCollections.observableArrayList(saleUtil.allWrappers(GlobalValues.getCurrentSubject().getId())));
    }

    public void filter() {
        String query = "select i from SaleItem i ";
        List<Object> params = new ArrayList<>();
        boolean isSelected = false;
        int arg = 1;

        if (startDateCB.isSelected() && startDateDP.getValue() != null) {
            query += " where FUNC('DATE', i.date / 1000, 'unixepoch') >= ?"+arg;
            arg++;
            params.add(startDateDP.getValue());
            isSelected = true;
        }
        if(endDateCB.isSelected() && endDateDP.getValue() != null){
            query += isSelected ? " and FUNC('DATE', i.date  / 1000, 'unixepoch') <= ?2" : " where FUNC('DATE', i.date  / 1000, 'unixepoch') >= ?"+arg;
            arg++;
            isSelected = true;
            params.add(endDateDP.getValue().toString());
        }

        List<SaleItem> filteredSales = saleUtil.filteredSales(query,params);
        List<SaleWrapper> saleWrappers = new ArrayList<>();
        for (SaleItem saleItem : filteredSales){
            SaleWrapper wrap = new SaleWrapper(saleItem);
            saleWrappers.add(wrap);
        }

        saleTable.setItems(FXCollections.observableArrayList(saleWrappers));
    }


}
