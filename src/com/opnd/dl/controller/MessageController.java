package com.opnd.dl.controller;

import com.opnd.dl.mStuff.GlobalValues;
import javafx.fxml.FXML;
import javafx.scene.control.Label;

public class MessageController {
    @FXML private Label username;
    @FXML private Label role;

    public void initialize(){
        if(GlobalValues.getCurrentSubject() != null){
            this.username.setText(GlobalValues.getCurrentSubject().getFullname());
            this.role.setText("Warehouse");
        }
    }
}
