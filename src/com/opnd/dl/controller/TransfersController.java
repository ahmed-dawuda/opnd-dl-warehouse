package com.opnd.dl.controller;

import com.opnd.dl.domain.Product;
import com.opnd.dl.domain.Requisition;
import com.opnd.dl.domain.Subject;
import com.opnd.dl.domain.TransferItem;
import com.opnd.dl.domain.wrapper.DetailWrapper;
import com.opnd.dl.domain.wrapper.TransferRequestWrapper;
import com.opnd.dl.mStuff.GlobalValues;
import com.opnd.dl.mStuff.Persistor;
import com.opnd.dl.mStuff.ViewHelper;
import com.opnd.dl.model.RequisitionUtil;
import com.opnd.dl.model.TransferUtil;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class TransfersController {
    @FXML private TableView<TransferRequestWrapper> requestTable;
    @FXML private TableColumn<TransferRequestWrapper,Date> dateCol;
    @FXML private TableColumn<TransferRequestWrapper, Subject> requestedByCol;
    @FXML private TableColumn<TransferRequestWrapper, Requisition.RequisitionStatus> statusCol;
    @FXML private TableColumn<TransferRequestWrapper, Date> responseDateCol;

    @FXML private TableView<DetailWrapper> detailTable;
    @FXML private TableColumn<DetailWrapper, Product> productCol;
    @FXML private TableColumn<DetailWrapper, Double> quantityCol;
    @FXML private TableColumn<DetailWrapper, Double> amountCol;
    @FXML private Label username;
    @FXML private Label role;
    RequisitionUtil requisitionUtil = new RequisitionUtil();

    private TransferUtil transferUtil = new TransferUtil();

    public void initialize(){
        if(GlobalValues.getCurrentSubject() != null){
            this.username.setText(GlobalValues.getCurrentSubject().getFullname());
            this.role.setText("Warehouse");
        }


        DecimalFormat df = new DecimalFormat("#,###,###.##");
        dateCol.setCellValueFactory(new PropertyValueFactory<>("date"));
        requestedByCol.setCellValueFactory(new PropertyValueFactory<>("requestedBy"));
        statusCol.setCellValueFactory(new PropertyValueFactory<>("status"));
        responseDateCol.setCellValueFactory(new PropertyValueFactory<>("responseDate"));

        productCol.setCellValueFactory(new PropertyValueFactory<>("product"));
        quantityCol.setCellValueFactory(new PropertyValueFactory<>("quantity"));
        amountCol.setCellValueFactory(new PropertyValueFactory<>("amount"));

        dateCol.setCellFactory(param -> new TableCell<TransferRequestWrapper, Date>(){
            @Override
            protected void updateItem(Date item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : DateFormat.getDateInstance().format(item));
            }
        });

        requestedByCol.setCellFactory(param -> new TableCell<TransferRequestWrapper, Subject>(){
            @Override
            protected void updateItem(Subject item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : item.getFullname());
            }
        });


        statusCol.setCellFactory(param -> new TableCell<TransferRequestWrapper, Requisition.RequisitionStatus>(){
            @Override
            protected void updateItem(Requisition.RequisitionStatus item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : item + "");
            }
        });


        responseDateCol.setCellFactory(param -> new TableCell<TransferRequestWrapper, Date>(){
            @Override
            protected void updateItem(Date item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : DateFormat.getDateInstance().format(item));
            }
        });

        productCol.setCellFactory(param -> new TableCell<DetailWrapper, Product>(){
            @Override
            protected void updateItem(Product item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : item.getDescription());
            }
        });

        quantityCol.setCellFactory(param -> new TableCell<DetailWrapper, Double>(){
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : df.format(item));
            }
        });

        amountCol.setCellFactory(param -> new TableCell<DetailWrapper, Double>(){
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : df.format(item));
            }
        });

        detailTable.setItems(FXCollections.observableArrayList());
        requestTable.setItems(FXCollections.observableArrayList(transferUtil.wrappedRequisitions()));

        requestTable.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
            System.out.println("clicked");
            TransferRequestWrapper item =  requestTable.getSelectionModel().getSelectedItem();
            List<TransferItem> transferItems = Persistor.fetch("select t from TransferItem t where t.requisitionId = ?1",item.getRequisition().getId());
            List<DetailWrapper> detailWrappers = new ArrayList<>();

            for(TransferItem transferItem : transferItems){
                DetailWrapper wrapper = new DetailWrapper(transferItem);
                System.out.println(wrapper.toString());
                detailWrappers.add(wrapper);
            }

            detailTable.setItems(FXCollections.observableArrayList(detailWrappers));

        });
    }

    @FXML private void issue(){

        if(requestTable.getSelectionModel().getSelectedItem() != null) {
            Requisition requisition = requestTable.getSelectionModel().getSelectedItem().getRequisition();
            requisitionUtil.issue(requisition);
            requestTable.getItems().remove(requestTable.getSelectionModel().getSelectedItem());
            detailTable.getItems().clear();
        }
    }

    @FXML private void flag(){
        if(requestTable.getSelectionModel().getSelectedItem() != null) {
            Persistor.begin();
            requestTable.getSelectionModel().getSelectedItem().getRequisition().setStatus(Requisition.RequisitionStatus.FLAGGED);
            Persistor.commit();
            requestTable.getItems().remove(requestTable.getSelectionModel().getSelectedItem());
            detailTable.getItems().clear();
        }
    }

    public void makeRequest(){
        new ViewHelper().makeModal("makeRequest");
    }
}
