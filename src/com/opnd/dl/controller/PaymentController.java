package com.opnd.dl.controller;

import com.opnd.dl.domain.wrapper.PaymentWrapper;
import com.opnd.dl.mStuff.ViewHelper;
import com.opnd.dl.model.PaymentUtil;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.util.Date;

public class PaymentController {

    @FXML private TableView<PaymentWrapper> paymentsTable;
    @FXML private TableColumn<PaymentWrapper, Date> dateCol;
    @FXML private TableColumn<PaymentWrapper, Date> timeCol;
    @FXML private TableColumn<PaymentWrapper, Double> amountCol;
    private PaymentUtil paymentUtil = new PaymentUtil();

    public void initialize(){
        dateCol.setCellValueFactory(new PropertyValueFactory<>("date"));
        timeCol.setCellValueFactory(new PropertyValueFactory<>("date"));
        amountCol.setCellValueFactory(new PropertyValueFactory<>("amount"));

        dateCol.setCellFactory(param -> new TableCell<PaymentWrapper, Date>(){
            @Override
            protected void updateItem(Date item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : DateFormat.getDateInstance().format(item));
            }
        });

        timeCol.setCellFactory(param -> new TableCell<PaymentWrapper, Date>(){
            @Override
            protected void updateItem(Date item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : DateFormat.getTimeInstance().format(item));
            }
        });

        amountCol.setCellFactory(param -> new TableCell<PaymentWrapper, Double>(){
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : (new DecimalFormat("#,###,###.##")).format(item));
            }
        });

        paymentsTable.setItems(FXCollections.observableArrayList(paymentUtil.wrappedPayments()));
    }

    public void payment(){
        ViewHelper viewHelper = new ViewHelper();
        System.out.println("payment clicked");
        viewHelper.makeModal("makePayment");
        paymentsTable.setItems(FXCollections.observableArrayList(paymentUtil.wrappedPayments()));
    }
}
