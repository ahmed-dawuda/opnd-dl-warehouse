package com.opnd.dl.controller;

import com.jfoenix.controls.JFXCheckBox;
import com.jfoenix.controls.JFXComboBox;
import com.opnd.dl.domain.Category;
import com.opnd.dl.domain.InventoryItem;
import com.opnd.dl.domain.Product;
import com.opnd.dl.domain.Subject;
import com.opnd.dl.domain.wrapper.InventoryWrapper;
import com.opnd.dl.mStuff.GlobalValues;
import com.opnd.dl.mStuff.Persistor;
import com.opnd.dl.model.InventoryUtil;
import javafx.beans.binding.Bindings;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import org.eclipse.persistence.jpa.jpql.parser.UpdateItem;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class InventoryController {

    @FXML private JFXCheckBox startDateCB;
    @FXML private DatePicker startDateDP;
    @FXML private DatePicker endDateDP;
    @FXML private JFXCheckBox endDateCB;
    @FXML private Label username;
    @FXML private Label role;



    @FXML private TableView<InventoryWrapper> inventoryTable;
    @FXML private TableColumn<InventoryWrapper, Date> dateCol;
    @FXML private TableColumn<InventoryWrapper, Product> productCol;
    @FXML private TableColumn<InventoryWrapper, Category> categoryCol;
    @FXML private TableColumn<InventoryWrapper, Double> quantityCol;
    @FXML private TableColumn<InventoryWrapper, Double> unitPriceCol;
    @FXML private TableColumn<InventoryWrapper, Double> amountCol;

    private InventoryUtil inventoryUtil = new InventoryUtil();

    public void initialize(){

        if(GlobalValues.getCurrentSubject() != null){
            this.username.setText(GlobalValues.getCurrentSubject().getFullname());
            this.role.setText("Warehouse");
        }

        startDateDP.disableProperty().bind(startDateCB.selectedProperty().not());
        endDateDP.disableProperty().bind(endDateCB.selectedProperty().not());
//        locationCMB.disableProperty().bind(locationCB.selectedProperty().not());
//        salespersonCMB.disableProperty().bind(salespersonCB.selectedProperty().not());
//        startDateDP.setDisable(true);

        dateCol.setCellValueFactory(new PropertyValueFactory<>("date"));
        productCol.setCellValueFactory(new PropertyValueFactory<>("product"));
        categoryCol.setCellValueFactory(new PropertyValueFactory<>("category"));
        quantityCol.setCellValueFactory(new PropertyValueFactory<>("quantity"));
        unitPriceCol.setCellValueFactory(new PropertyValueFactory<>("unitprice"));
        amountCol.setCellValueFactory(new PropertyValueFactory<>("amount"));

        DecimalFormat df = new DecimalFormat("#,###,###.##");

        amountCol.setCellFactory(param -> new TableCell<InventoryWrapper, Double>() {
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty? null : df.format(item));
            }
        });

        categoryCol.setCellFactory(param -> new TableCell<InventoryWrapper, Category>(){
            @Override
            protected void updateItem(Category item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : item.getDescription());
            }
        });

        quantityCol.setCellFactory(param -> new TableCell<InventoryWrapper, Double>(){
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : df.format(item));
            }
        });

        quantityCol.setCellFactory(param -> new TableCell<InventoryWrapper, Double>(){
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : df.format(item));
            }
        });

        productCol.setCellFactory(param -> new TableCell<InventoryWrapper, Product>(){
            @Override
            protected void updateItem(Product item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : item.getDescription());
            }
        });

        unitPriceCol.setCellFactory(param -> new TableCell<InventoryWrapper, Double>(){
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : df.format(item));
            }
        });

        dateCol.setCellFactory(param -> new TableCell<InventoryWrapper, Date>(){
            @Override
            protected void updateItem(Date item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : DateFormat.getDateInstance().format(item));
            }
        });

        List inventories = inventoryUtil.inventories();
        List<InventoryWrapper> inventoryItems = new ArrayList<>();
        for(Object inventoryItem : inventories){
            inventoryItems.add(new InventoryWrapper((InventoryItem) inventoryItem));
        }

        inventoryTable.setItems(FXCollections.observableArrayList(inventoryItems));

    }

    public void filter() {
        InventoryUtil inventoryUtil = new InventoryUtil();
        String query = "select i from InventoryItem i ";
        List<Object> params = new ArrayList<>();
        boolean isSelected = false;
        int arg = 1;

        if (startDateCB.isSelected() && startDateDP.getValue() != null) {
            query += " where FUNC('DATE', i.date / 1000, 'unixepoch') >= ?"+arg;
            arg++;
            params.add(startDateDP.getValue());
            isSelected = true;
        }
        if(endDateCB.isSelected() && endDateDP.getValue() != null){
            query += isSelected ? " and FUNC('DATE', i.date  / 1000, 'unixepoch') <= ?2" : " where FUNC('DATE', i.date  / 1000, 'unixepoch') >= ?"+arg;
            arg++;
            isSelected = true;
            params.add(endDateDP.getValue().toString());
        }

        List<InventoryItem> filteredInventories = inventoryUtil.filteredInventories(query,params);
        List<InventoryWrapper> inventoryWrappers = new ArrayList<>();
        for (InventoryItem inventoryItem : filteredInventories){
            InventoryWrapper wrap = new InventoryWrapper(inventoryItem);
            inventoryWrappers.add(wrap);
        }

        inventoryTable.setItems(FXCollections.observableArrayList(inventoryWrappers));
    }
}
