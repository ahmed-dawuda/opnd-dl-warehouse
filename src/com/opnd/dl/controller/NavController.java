package com.opnd.dl.controller;

import com.opnd.dl.mStuff.ViewHelper;
import com.opnd.dl.model.EditUtil;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class NavController {

    @FXML private VBox mainNode;
    private ViewHelper viewHelper;
    @FXML private Button maximise;

    public void initialize(){
        this.viewHelper = new ViewHelper();
    }

    public void home(){ viewHelper.switchRoot("home",mainNode);}

    public void sale(){
        viewHelper.switchRoot("sale",mainNode);
    }

    public void inventory(){
        viewHelper.switchRoot("inventory",mainNode);
    }

    public void messaging(){
        viewHelper.switchRoot("messaging",mainNode);
    }

    public void transfers(){
        viewHelper.switchRoot("transfers",mainNode);
    }

    public void newStock(){
        viewHelper.switchRoot("newstock",mainNode);
    }

    public void logout(){
        viewHelper.switchScene("login",false,mainNode);
    }

    @FXML private void max(){
        ( (Stage) maximise.getScene().getWindow()).setMaximized(true);
    }

    @FXML private void close(){
        Platform.exit();
    }

    public void payments(){
        viewHelper.switchRoot("payment", mainNode);
    }
}
