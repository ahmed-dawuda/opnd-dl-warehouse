package com.opnd.dl.controller;

import com.opnd.dl.mStuff.GlobalValues;
import com.opnd.dl.mStuff.ViewHelper;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class HomeController {

    @FXML private VBox anchorNode;
    private ViewHelper viewHelper;
    @FXML private Button maximise;
    @FXML private Label username;
    @FXML private Label role;

    public void initialize(){
        this.viewHelper = new ViewHelper();
        if(GlobalValues.getCurrentSubject() != null){
            this.username.setText(GlobalValues.getCurrentSubject().getFullname());
            this.role.setText("Warehouse");
        }
    }

    public void home(){ viewHelper.switchRoot("home",anchorNode);}

    public void sale(){
        viewHelper.switchRoot("sale",anchorNode);
    }

    public void inventory(){
        viewHelper.switchRoot("inventory",anchorNode);
    }

    public void messaging(){
        viewHelper.switchRoot("messaging",anchorNode);
    }

    public void transfers(){
        viewHelper.switchRoot("transfers",anchorNode);
    }

    public void newStock(){
        viewHelper.switchRoot("newstock",anchorNode);
    }

    public void logout(){ viewHelper.switchScene("login",false,anchorNode); }

}
