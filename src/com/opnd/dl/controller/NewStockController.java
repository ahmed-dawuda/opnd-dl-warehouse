package com.opnd.dl.controller;

import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.opnd.dl.domain.InventoryItem;
import com.opnd.dl.domain.Product;
import com.opnd.dl.mStuff.GlobalValues;
import com.opnd.dl.mStuff.Persistor;
import com.opnd.dl.model.InventoryUtil;
import com.opnd.dl.model.ProductUtil;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import com.opnd.dl.domain.NewStockItem;
import java.text.DecimalFormat;
import java.time.Instant;
import java.util.Date;
import java.util.List;

public class NewStockController {

    //form fields
    @FXML private JFXTextField amount;
    @FXML private JFXTextField quantity;
    @FXML private JFXComboBox<Product> product;

    //table
    @FXML private TableView<NewStockItem> stockTable;
    @FXML private TableColumn<NewStockItem, Product> productCol;
    @FXML private TableColumn<NewStockItem, Double> amountCol;
    @FXML private TableColumn<NewStockItem, Double> quantityCol;
    @FXML private TableColumn<NewStockItem, Product> deleteCol;

    @FXML private Label username;
    @FXML private Label role;

    private InventoryUtil inventoryUtil = new InventoryUtil();

    private double qty = 0;
    private double amt = 0;
    private Product p;

    //initializing
    public void initialize(){
        if(GlobalValues.getCurrentSubject() != null){
            this.username.setText(GlobalValues.getCurrentSubject().getFullname());
            this.role.setText("Warehouse");
        }
        productCol.setCellValueFactory(new PropertyValueFactory<>("product"));
        amountCol.setCellValueFactory(new PropertyValueFactory<>("amount"));
        quantityCol.setCellValueFactory(new PropertyValueFactory<>("quantity"));
        deleteCol.setCellValueFactory(new PropertyValueFactory<>("product"));

        DecimalFormat df = new DecimalFormat("#,###,###.##");

        amountCol.setCellFactory(param -> new TableCell<NewStockItem, Double>(){
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText((item == null || empty) ? null : df.format(item));
            }
        });

        quantityCol.setCellFactory(param -> new TableCell<NewStockItem, Double>(){
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText((item == null || empty) ? null : df.format(item));
            }
        });

        productCol.setCellFactory(param -> new TableCell<NewStockItem, Product>() {
            @Override
            protected void updateItem(Product item, boolean empty) {
                super.updateItem(item, empty);
                setText((item == null || empty) ? null : item.getDescription());
            }
        });

        deleteCol.setCellFactory(param -> new TableCell<NewStockItem, Product>(){
            @Override
            protected void updateItem(Product item, boolean empty) {
                super.updateItem(item, empty);
                Hyperlink deletLink =  new Hyperlink("DELETE");
                deletLink.setOnAction(e -> deleteRow(item));
                deletLink.setStyle("-fx-alignment: baseline-center");
                setGraphic(item == null || empty ? null : deletLink);
            }
        });

        product.setCellFactory(param -> new ListCell<Product>(){
            @Override
            protected void updateItem(Product item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : item.getDescription());
            }
        });


        quantity.textProperty().addListener((observable, oldValue, newValue) -> {
            newValue = newValue.isEmpty() ? "0" : newValue;

            if (!newValue.matches("^[+-]?\\d+$")) {
                quantity.setText(oldValue);
                return;
            }

            qty = Double.parseDouble(newValue);
            amt = qty * p.getPrice();
            amount.setText(df.format(amt));
        });

        amount.setEditable(false);
        product.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> p = newValue);

//        ProductUtil util = new ProductUtil();
//        product.setItems(FXCollections.observableArrayList(util.allProducts()));
        ProductUtil productUtil = new ProductUtil();
        product.setItems(FXCollections.observableArrayList(productUtil.products()));
//        if (product.getItems().size() > 0) product.getSelectionModel().select(0);

        stockTable.setItems(FXCollections.observableArrayList());

//        stockTable.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
//            System.out.println("clicked");
//            NewStockItem item =  stockTable.getSelectionModel().getSelectedItem();
//            System.out.println(item.getProduct().getDescription());
//        });
    }


    @FXML private void deleteRow(Product item){
        for (NewStockItem newStockItem : stockTable.getItems()) {
            if(newStockItem.getProduct().getId() == item.getId()){
                stockTable.getItems().remove(newStockItem);
            }
        }
    }

    @FXML private void add(){
        if(amt != 0 && qty != 0 && p != null){
            NewStockItem item = new NewStockItem();
            item.setAmount(amt);
            item.setProduct(p);
            item.setQuantity(qty);
            stockTable.getItems().add(item);
            this.quantity.setText("");
            this.amount.setText("");
        }
    }

    @FXML private void cancel(){
        stockTable.getItems().clear();
    }

    @FXML private void finish(){

        List<NewStockItem> newStockItems = stockTable.getItems();
        for (NewStockItem newStockItem : newStockItems) {
            List<InventoryItem> items = Persistor.fetch("Select i from InventoryItem i where i.productId = ?1", newStockItem.getProduct().getId());
            if(items.size() > 0){
                InventoryItem item = items.get(0);
                Persistor.begin();
                item.setQuantity(items.get(0).getQuantity() + newStockItem.getQuantity());
                item.setUpdated(false);
                Persistor.commit();
            }else{
                inventoryUtil.insert(newStockItem.getProduct().getId(),newStockItem.getQuantity());
            }
        }
        cancel();
    }
}
